package com.example.upload_example.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.upload_example.entities.UserEntity;
import com.example.upload_example.repositories.UserRepository;
import com.example.upload_example.services.FileHandler;

@Controller
@RequestMapping(value = "/")
public class UploadController {

	private final Logger logger = (Logger) LoggerFactory.getLogger(this.getClass());

	@Autowired
	private FileHandler fileHandler;

	@Autowired
	private UserRepository userRepository;

	@RequestMapping(method = RequestMethod.GET)
	public String index() {
		return "upload";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/uploadStatus")
	public String uploadStatus() {
		return "uploadStatus";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/upload")
	public String singleFileUpload(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) {
		logger.debug("This is a debug message");
		logger.info("this is a info message");
		String result = null;
		try {
			result = fileHandler.singleFileUpload(file, redirectAttributes);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	@GetMapping(value = "/users")
	@ResponseBody
	public List<UserEntity> getUsers() {
		return (List<UserEntity>) userRepository.findAll();
	}

	@GetMapping(value = "/download")
	public ResponseEntity<Resource> downloadUsersList() {
		try {
			File file = fileHandler.writeUsersListToFile();
			
			InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
			
			HttpHeaders responseHeaders = new HttpHeaders();
	        responseHeaders.add("content-disposition", "attachment; filename=" + "userList.txt");
	        
			
			return ResponseEntity.ok()
		            .headers(responseHeaders)
		            .contentLength(file.length())
		            .contentType(MediaType.parseMediaType("application/octet-stream"))
		            .body(resource);
		} catch (IOException e) {
			e.getStackTrace();
		}
		return null;
	}

}
