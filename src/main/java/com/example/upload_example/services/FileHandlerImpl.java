package com.example.upload_example.services;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.upload_example.entities.UserEntity;
import com.example.upload_example.repositories.UserRepository;

@Service
public class FileHandlerImpl implements FileHandler {

	@Autowired
	private UserRepository userRepository;

	@Override
	public String singleFileUpload(MultipartFile file, RedirectAttributes redirectAttributes) throws IOException {

		if (file.isEmpty()) {
			redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
			return "redirect:uploadStatus";
		}
		try {

			File temp = File.createTempFile("temp", ".txt");

			byte[] bytes = file.getBytes();
			Files.write(temp.toPath(), bytes);

			List<String> userList = Files.readAllLines(temp.toPath());

			for (String user : userList) {
				String[] attributes = user.split(",");
				if (attributes.length == 4 && !(userRepository.existsByEmail(attributes[2]))) {
					UserEntity userEntity = new UserEntity();
					userEntity.setFirstName(attributes[0]);
					userEntity.setLastName(attributes[1]);
					userEntity.setEmail(attributes[2]);
					userEntity.setCity(attributes[3]);
					if (attributes[3].equals("Novi Sad")) {
						userEntity.setExpenses(5000);
					} else if (attributes[3].equals("Beograd")) {
						userEntity.setExpenses(10000);
					} else {
						userEntity.setExpenses(0);
					}
					userRepository.save(userEntity);
				}
			}

			temp.deleteOnExit();

			redirectAttributes.addFlashAttribute("message",
					"You successfully uploaded '" + file.getOriginalFilename() + "'");

		} catch (IOException e) {
			throw e;
		}

		return "redirect:uploadStatus";
	}

	@Override
	public File writeUsersListToFile() {
		try {
			
			File temp = File.createTempFile("list", ".txt");
			BufferedWriter writer = new BufferedWriter(new FileWriter(temp));
			
			List<UserEntity> userList = (List<UserEntity>) userRepository.findAll();
			
			for (UserEntity user : userList) {
				writer.write(user.toString());
				writer.newLine();
			}
			
			writer.close();
			
			return temp;
			
		} catch (IOException e) {
			e.getStackTrace();
		}
		return null;
	}

}
