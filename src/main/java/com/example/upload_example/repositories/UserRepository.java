package com.example.upload_example.repositories;

import org.springframework.data.repository.CrudRepository;

import com.example.upload_example.entities.UserEntity;

public interface UserRepository extends CrudRepository<UserEntity, Integer> {
	
	Boolean existsByEmail(String email);

}
